import labmem.Array.Array;
import labmem.Set.BSTSet;
import book.FileOperation;

/**
 * 使用文本单词数测试BSTSet
 */
public class TestBSTSet {

    public static void main(String[] args) {

        System.out.println("Pride and Prejudice");//傲慢与偏见

        //使用array记录单词数
        Array<String> words1 = new Array<>();
        FileOperation.readFile("src/book/pride-and-prejudice.txt", words1);
        System.out.println("Total words:" + words1.getSize());

        //使用set记录单词数,为不重复单词数
        BSTSet<String> set1 = new BSTSet<>();
        for (int i = 0; i < words1.getSize(); i++)
            set1.add(words1.get(i));
        System.out.println("Total different words:" + set1.getSize());


    }

}
