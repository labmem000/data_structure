import labmem.Set.BSTSet;
import labmem.Trie.Trie;

/**
 * 使用BSTSet和trie进行性能比较
 * trie在数据越多的情况下性能更好
 * @author Labmem-刘天予
 * @date 2019/1/10
 */
public class TestBSTSet_vs_Trie {
    public static void main(String[] args) {

        long startTime = System.nanoTime();

        BSTSet<String> set = new BSTSet<>();
        for (int i = 10000; i >= 0; i--)
            set.add(String.valueOf(i));

        for (int i = 10000; i >= 0; i--)
            set.contains(String.valueOf(i));

        long endTime = System.nanoTime();

        double time = (endTime - startTime) / 1000000000.0;

        System.out.println("Total different words: " + set.getSize());
        System.out.println("BSTSet: " + time + " s");

        // ---

        startTime = System.nanoTime();

        Trie trie = new Trie();
        for (int i = 10000; i >= 0; i--)
            trie.add(String.valueOf(i));

        for (int i = 10000; i >= 0; i--)
            trie.contains(String.valueOf(i));

        endTime = System.nanoTime();

        time = (endTime - startTime) / 1000000000.0;

        System.out.println("Total different words: " + trie.getSize());
        System.out.println("Trie: " + time + " s");
    }
}
