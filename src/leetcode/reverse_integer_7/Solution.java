package leetcode.reverse_integer_7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 整数反转
 * <p>
 * 给出一个 32 位的有符号整数，你需要将这个整数中每位上的数字进行反转。
 */
public class Solution {

    public static int reverse(int x) {
        System.out.println("输入：" + x);

        String out = "";
        char[] chars = String.valueOf(x).toCharArray();

        if (x < 0) {
            out = "-";
            for (int i = chars.length - 1; i > 0; i--) {
                out += chars[i];
            }
            try {
                int reverse = Integer.parseInt(out);
                return reverse;
            } catch (NumberFormatException e) {
                return 0;
            }
        } else {
            for (int i = chars.length - 1; i >= 0; i--) {
                out += chars[i];
            }
            try {
                int reverse = Integer.parseInt(out);
                return reverse;
            } catch (NumberFormatException e) {
                return 0;
            }
        }
    }

    public static void main(String[] args) {

        System.out.println("" + reverse1(1534236469));

//        System.out.println("" + 0 % 10);
    }

    public static int reverse1(int x) {
        //最开始想的笨比反转，突然发现反过来不就是膜吗
//        StringBuilder out=new StringBuilder(String.valueOf(x));
//        if (x<0) {
//            out.append("-");
//            out.reverse();
//            out.deleteCharAt(out.length()-1);
//            try {
//                return Integer.parseInt(out.toString());
//            }catch (NumberFormatException e) {
//                return 0;
//            }
//        } else {
//            out.reverse();
//            try {
//                return Integer.parseInt(out.toString());
//            }catch (NumberFormatException e) {
//                return 0;
//            }
//        }
        long sum = 0;
        while (x != 0) {
            System.out.println("%" + x % 10);
            sum = (x % 10) + (sum * 10);
            System.out.println("sum" + sum);
            x = x / 10;
        }
        if (sum > Integer.MAX_VALUE)
            return 0;
        if (sum < Integer.MIN_VALUE)
            return 0;
        return (int) sum;

    }

}
