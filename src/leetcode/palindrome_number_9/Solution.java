package leetcode.palindrome_number_9;

/**
 * 回文数
 * <p>
 * 判断一个整数是否是回文数。回文数是指正序（从左向右）和倒序（从右向左）读都是一样的整数。
 */
public class Solution {

    public static boolean isPalindrome(int x) {
        if (x < 0)
            return false;
        int xx = x;
        long sum = 0;
        while (x != 0) {
            sum = (x % 10) + (sum * 10);
            x = x / 10;
        }
        return sum == xx;

    }

    public static void main(String[] args) {
        System.out.println(isPalindrome(0));
    }
}
