package leetcode.two_sum_1;

import labmem.Map.LinkedListMap;

/**
 * 两数之和
 *
 * 给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的那 两个 整数，并返回他们的数组下标。
 *
 * 你可以假设每种输入只会对应一个答案。但是，你不能重复利用这个数组中同样的元素。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/two-sum
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution {

    public static int[] twoSum(int[] nums, int target) {

        LinkedListMap<Integer, Integer> map = new LinkedListMap<>();
        for (int i = 0; i < nums.length; i++) {
            int complement=target-nums[i];
                    if (map.contains(complement))
                        return new int[] {map.get(complement),i};
            map.add(nums[i], i);
        }
        throw new IllegalArgumentException("No two sum solution");

    }

    public static void main(String[] args) {
        int[] nums = {3, 3};
        int[] index = twoSum(nums, 6);
        System.out.println(index[0] + "," + index[1]);
    }

}
