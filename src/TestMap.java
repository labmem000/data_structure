import book.FileOperation;
import labmem.Array.Array;
import labmem.Map.BSTMap;
import labmem.Map.LinkedListMap;
import labmem.Map.Map;

/**
 * 测试BSTMap和LinkedListMap的性能
 *
 * @author Labmem-刘天予
 * @date 2019/1/8
 */
public class TestMap {

    private static double testMap(Map<String, Integer> map, String filename) {

        long startTime = System.nanoTime();

        System.out.println(filename);
        Array<String> words = new Array<>();
        if (FileOperation.readFile(filename, words)) {
            System.out.println("Total words:" + words.getSize());
            for (int i = 0; i < words.getSize(); i++) {
                String word = words.get(i);
                if (map.contains(word))
                    map.set(word, map.get(word) + 1);
                else
                    map.add(word, 1);
            }
            System.out.println("Total different words:" + map.getSize());
            System.out.println("Frequency of PRIDE:" + map.get("pride"));
            System.out.println("Frequency of PREJUDICE:" + map.get("prejudice"));
        }
        long endTime = System.nanoTime();

        return (endTime - startTime) / 1000000000.0;
    }

    public static void main(String[] args) {
        String filename = "src/book/pride-and-prejudice.txt";

        BSTMap<String, Integer> bstMap = new BSTMap<>();
        double time1 = testMap(bstMap, filename);
        System.out.println("BST Map:" + time1 + "s");

        System.out.println();

        LinkedListMap<String, Integer> linkedListMap = new LinkedListMap<>();
        double time2 = testMap(linkedListMap, filename);
        System.out.println("BST Map:" + time2 + "s");
    }

}
