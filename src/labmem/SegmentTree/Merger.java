package labmem.SegmentTree;

/**
 * 融合器
 *
 * @author Labmem-刘天予
 * @date 2019/1/10
 */
public interface Merger<E> {
    E merge(E a, E b);
}
