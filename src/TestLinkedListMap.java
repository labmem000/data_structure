import book.FileOperation;
import labmem.Array.Array;
import labmem.Map.LinkedListMap;

public class TestLinkedListMap {

    public static void main(String[] args) {

        System.out.println("Pride and Prejudice");//傲慢与偏见

        //使用array记录单词数
        Array<String> words = new Array<>();
        if (FileOperation.readFile("src/book/pride-and-prejudice.txt", words)) {
            System.out.println("Total words:" + words.getSize());

            //使用map记录每个单词的频率
            LinkedListMap<String, Integer> map = new LinkedListMap<>();
            for (int i = 0; i < words.getSize(); i++) {
                String word = words.get(i);
                if (map.contains(word))
                    map.set(word, map.get(word) + 1);
                else
                    map.add(word, 1);
            }
            System.out.println("Total different words:" + map.getSize());
            //查出pride出现的频率
            System.out.println("Frequency of PRIDE:" + map.get("pride"));

        }
    }

}
